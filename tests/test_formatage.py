import unittest
import csv
import os
from collections import OrderedDict
from formatage import lecture
from formatage import divisionTypeVarianteVersion
from formatage import changerFormatDate
from formatage import changerFormat
from formatage import ecriture

class TestFormatageFunctions(unittest.TestCase):

    def setUp(self):
        with open("test.csv", 'w') as writefile:
            writefile.write('address|carrosserie|categorie|couleur|cylindree|date_immat|denomination|energy|firstname|immat|marque|name|places|poids|puissance|type_variante_version|vin\n')
            writefile.write('3822 Omar Square Suite 257 Port Emily, OK 43251|45-1743376|34-7904216|LightGoldenRodYellow|3462|2012-05-03|Enhanced well-modulated moderator|37578077|Jerome|OVC-568|Williams Inc|Smith|32|3827|110|Inc, 92-3625175, 79266482|9780082351764')

    def test_lecture(self):
        donnees = lecture("test.csv")
        test = OrderedDict([('address', '3822 Omar Square Suite 257 Port Emily, OK 43251'), ('carrosserie', '45-1743376'), ('categorie', '34-7904216'), ('couleur', 'LightGoldenRodYellow'), ('cylindree', '3462'), ('date_immat', '2012-05-03'), ('denomination', 'Enhanced well-modulated moderator'), ('energy', '37578077'), ('firstname', 'Jerome'), ('immat', 'OVC-568'), ('marque', 'Williams Inc'), ('name', 'Smith'), ('places', '32'), ('poids', '3827'), ('puissance', '110'), ('type_variante_version', 'Inc, 92-3625175, 79266482'), ('vin', '9780082351764')])
        self.assertEqual(test, donnees[0])

    def test_divisionTypeVarianteVersion(self):
        test = OrderedDict([('address', '3822 Omar Square Suite 257 Port Emily, OK 43251'), ('carrosserie', '45-1743376'), ('categorie', '34-7904216'), ('couleur', 'LightGoldenRodYellow'), ('cylindree', '3462'), ('date_immat', '2012-05-03'), ('denomination', 'Enhanced well-modulated moderator'), ('energy', '37578077'), ('firstname', 'Jerome'), ('immat', 'OVC-568'), ('marque', 'Williams Inc'), ('name', 'Smith'), ('places', '32'), ('poids', '3827'), ('puissance', '110'), ('type_variante_version', 'Inc, 92-3625175, 79266482'), ('vin', '9780082351764')])
        liste = [test]
        test2 = OrderedDict([('address', '3822 Omar Square Suite 257 Port Emily, OK 43251'), ('carrosserie', '45-1743376'), ('categorie', '34-7904216'), ('couleur', 'LightGoldenRodYellow'), ('cylindree', '3462'), ('date_immat', '2012-05-03'), ('denomination', 'Enhanced well-modulated moderator'), ('energy', '37578077'), ('firstname', 'Jerome'), ('immat', 'OVC-568'), ('marque', 'Williams Inc'), ('name', 'Smith'), ('places', '32'), ('poids', '3827'), ('puissance', '110'), ('vin', '9780082351764'), ('type', 'Inc'), ('variante', '92-3625175'), ('version', '79266482')])
        liste2 = [test2]
        divisionTypeVarianteVersion(liste)
        self.assertEqual(liste2, liste)

    def test_changerFormatDate(self):
        test1 = OrderedDict([('address', '3822 Omar Square Suite 257 Port Emily, OK 43251'), ('carrosserie', '45-1743376'), ('categorie', '34-7904216'), ('couleur', 'LightGoldenRodYellow'), ('cylindree', '3462'), ('date_immat', '2012-05-03'), ('denomination', 'Enhanced well-modulated moderator'), ('energy', '37578077'), ('firstname', 'Jerome'), ('immat', 'OVC-568'), ('marque', 'Williams Inc'), ('name', 'Smith'), ('places', '32'), ('poids', '3827'), ('puissance', '110'), ('vin', '9780082351764'), ('type', 'Inc'), ('variante', '92-3625175'), ('version', '79266482')])
        liste = [test1]
        test2 = OrderedDict([('address', '3822 Omar Square Suite 257 Port Emily, OK 43251'), ('carrosserie', '45-1743376'), ('categorie', '34-7904216'), ('couleur', 'LightGoldenRodYellow'), ('cylindree', '3462'), ('date_immat', '03/05/2012'), ('denomination', 'Enhanced well-modulated moderator'), ('energy', '37578077'), ('firstname', 'Jerome'), ('immat', 'OVC-568'), ('marque', 'Williams Inc'), ('name', 'Smith'), ('places', '32'), ('poids', '3827'), ('puissance', '110'), ('vin', '9780082351764'), ('type', 'Inc'), ('variante', '92-3625175'), ('version', '79266482')])
        liste2 = [test2]
        changerFormatDate(liste)
        self.assertEqual(liste2, liste)

    def test_changerFormat(self):
        test1 = OrderedDict([('address', '3822 Omar Square Suite 257 Port Emily, OK 43251'), ('carrosserie', '45-1743376'), ('categorie', '34-7904216'), ('couleur', 'LightGoldenRodYellow'), ('cylindree', '3462'), ('date_immat', '2012-05-03'), ('denomination', 'Enhanced well-modulated moderator'), ('energy', '37578077'), ('firstname', 'Jerome'), ('immat', 'OVC-568'), ('marque', 'Williams Inc'), ('name', 'Smith'), ('places', '32'), ('poids', '3827'), ('puissance', '110'), ('vin', '9780082351764'), ('type', 'Inc'), ('variante', '92-3625175'), ('version', '79266482')])
        liste = [test1]
        test2 = OrderedDict([('adresse_titulaire', '3822 Omar Square Suite 257 Port Emily, OK 43251'), ('nom', 'Smith'), ('prenom', 'Jerome'), ('immatriculation', 'OVC-568'), ('date_immatriculation', '2012-05-03'), ('vin', '9780082351764'), ('marque', 'Williams Inc'), ('denomination_commerciale', 'Enhanced well-modulated moderator'), ('couleur', 'LightGoldenRodYellow'), ('carroserie', '45-1743376'), ('categorie', '34-7904216'), ('cylindree', '3462'), ('energie', '37578077'), ('places', '32'), ('poids', '3827'), ('puissance', '110'), ('type', 'Inc'), ('variante', '92-3625175'), ('version', '79266482')])
        liste2 = [test2]
        tableauConfig = {"address":"adresse_titulaire", "name":"nom", "firstname":"prenom", "immat":"immatriculation", 
                    "date_immat":"date_immatriculation", "vin":"vin", "marque":"marque", "denomination":"denomination_commerciale", 
                    "couleur":"couleur", "carrosserie":"carroserie", "categorie":"categorie", "cylindree":"cylindree", 
                    "energy":"energie", "places":"places", "poids":"poids", "puissance":"puissance", "type":"type", 
                    "variante":"variante", "version":"version"}
        changerFormat(liste, tableauConfig)
        self.assertEqual(liste2, liste)


    def test_ecriture(self):
        test2 = OrderedDict([('adresse_titulaire', '3822 Omar Square Suite 257 Port Emily, OK 43251'), ('nom', 'Smith'), ('prenom', 'Jerome'), ('immatriculation', 'OVC-568'), ('date_immatriculation', '2012-05-03'), ('vin', '9780082351764'), ('marque', 'Williams Inc'), ('denomination_commerciale', 'Enhanced well-modulated moderator'), ('couleur', 'LightGoldenRodYellow'), ('carroserie', '45-1743376'), ('categorie', '34-7904216'), ('cylindree', '3462'), ('energie', '37578077'), ('places', '32'), ('poids', '3827'), ('puissance', '110'), ('type', 'Inc'), ('variante', '92-3625175'), ('version', '79266482')])
        liste2 = [test2]
        tableauConfig = {"address":"adresse_titulaire", "name":"nom", "firstname":"prenom", "immat":"immatriculation", 
                    "date_immat":"date_immatriculation", "vin":"vin", "marque":"marque", "denomination":"denomination_commerciale", 
                    "couleur":"couleur", "carrosserie":"carroserie", "categorie":"categorie", "cylindree":"cylindree", 
                    "energy":"energie", "places":"places", "poids":"poids", "puissance":"puissance", "type":"type", 
                    "variante":"variante", "version":"version"}
        ecriture("test2.csv", liste2, tableauConfig, ';')
        self.assertTrue(os.path.exists("test2.csv"))
        line1 = "adresse_titulaire;nom;prenom;immatriculation;date_immatriculation;vin;marque;denomination_commerciale;couleur;carroserie;categorie;cylindree;energie;places;poids;puissance;type;variante;version"
        line2 = "3822 Omar Square Suite 257 Port Emily, OK 43251;Smith;Jerome;OVC-568;2012-05-03;9780082351764;Williams Inc;Enhanced well-modulated moderator;LightGoldenRodYellow;45-1743376;34-7904216;3462;37578077;32;3827;110;Inc;92-3625175;79266482"
        with open("test2.csv") as readfile:
            lines = readfile.readlines()
        self.assertEqual(line1+'\n', lines[0])
        self.assertEqual(line2+'\n', lines[1])
            

    def tearDown(self):
        os.remove("test.csv")
        if(os.path.exists("test2.csv")):
            os.remove("test2.csv")