import csv
import argparse
import os
import datetime

"""Lis un fichier passé en paramètre
IN: nom du fichier
OUT: liste de dictionnaires ordonnés(un pour chaque ligne du fichier)
"""
def lecture(filename):
    with open(filename) as csvfile:
        donnees = []
        dictDonnees = csv.DictReader(csvfile, delimiter='|')
        for row in dictDonnees:
            donnees.append(row)
    return donnees
    
"""Division le champ type_variante_version et trois champs distincts
IN: la liste de dictionnaires ordonnés
OUT: la liste de dictionnaires ordonnés modifiés
"""
def divisionTypeVarianteVersion(listeDonnees):
    for orderedDict in listeDonnees:
        tvv = orderedDict['type_variante_version']
        del orderedDict['type_variante_version']
        parts = tvv.split(", ")
        orderedDict['type'] = parts[0]
        orderedDict['variante'] = parts[1]
        orderedDict['version'] = parts[2]

"""Modifie le nom des champs et leur ordre
IN: la liste de dictionnaires ordonnés, le tableau de configuration 
OUT: la liste de dictionnaires ordonnés modifiés
"""
def changerFormat(listeDonnees, tableauConfig):
    for donnees in listeDonnees:
        for field, new_field in tableauConfig.items():
            if(not(field == new_field)):
                donnees[new_field] = donnees.pop(field)
            donnees.move_to_end(new_field, last=True)

"""Modifie le format du champ date_immat dans les dictionnaires ordonnée de la liste
IN: la liste de dictionnaires ordonnés
OUT: la liste de dictionnaires ordonnés modifiés 
"""
def changerFormatDate(listeDonnees):
    for orderedDict in listeDonnees:
        date = datetime.datetime.strptime(orderedDict['date_immat'], '%Y-%m-%d')
        orderedDict['date_immat'] = date.strftime('%d/%m/%Y')

"""Ecrie le fichier csv correspond à une liste de dictionnaires ordonnés
IN: le nom du fichier,la liste de dictionnaires ordonnés, le tableau de configuration 
OUT: le fichier csv
"""
def ecriture(newfilename, listeDonnees, tableauConfig, delimiter):
    with open(newfilename, "w") as csvfile:
        writer = csv.DictWriter(csvfile, delimiter=delimiter, lineterminator='\n',fieldnames=tableauConfig.values())
        writer.writeheader()
        for row in listeDonnees:
            writer.writerow(row)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='format a csv file')
    parser.add_argument("filepath", type=str, help="path to the file to format")
    parser.add_argument("-d", "--delimiter", type=str, default=";", help="delimiter used in the new format")
    args = parser.parse_args()
    tableauConfig = {"address":"adresse_titulaire", "name":"nom", "firstname":"prenom", "immat":"immatriculation", 
                    "date_immat":"date_immatriculation", "vin":"vin", "marque":"marque", "denomination":"denomination_commerciale", 
                    "couleur":"couleur", "carrosserie":"carroserie", "categorie":"categorie", "cylindree":"cylindree", 
                    "energy":"energie", "places":"places", "poids":"poids", "puissance":"puissance", "type":"type", "variante":"variante", "version":"version"}
    if(os.path.exists(args.filepath) and os.path.isfile(args.filepath)):
        listeDonnees =  lecture(args.filepath)
        divisionTypeVarianteVersion(listeDonnees)
        changerFormatDate(listeDonnees)
        changerFormat(listeDonnees, tableauConfig)
        ecriture("NewFormat"+args.filepath.capitalize(), listeDonnees, tableauConfig, args.delimiter)
    else:
        print("Erreur fichier inexistant")